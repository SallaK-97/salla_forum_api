import request from 'supertest';
import express from 'express';
import router from '../src/router';
import { findAllUsers, findOneUser, addUser, findAllposts, addPost, addComment, deletePost, deleteUser } from "../src/dao"


jest.mock('../src/dao', () => ({
    findAllUsers: jest.fn(),
    findOneUser: jest.fn(),
    addUser: jest.fn(),
    findAllposts: jest.fn(),
    findOnePost: jest.fn(),
    addPost: jest.fn(),
    addComment: jest.fn(),
    deleteComment: jest.fn(),
    deletePost: jest.fn(),
    deleteUser: jest.fn(),
}));

const app = express();
app.use(express.json());
app.use('/', router);

//test all users
describe('API Endpoints', () => {
    describe('GET /users', () => {
        it('should return all users', async () => {
            const mockUsers = [
                { id: 1, username: 'user1' },
                { id: 2, username: 'user2' },
            ];
            (findAllUsers as jest.Mock).mockResolvedValue(mockUsers);

            const response = await request(app).get('/users');

            expect(response.status).toBe(200);
            expect(response.body).toEqual(mockUsers);
        });
    });

    //test search user with id
    describe('GET /users/:id', () => {
        it('should return the user with the specified id', async () => {
            const mockUser = { id: 1, username: 'user1' };
            (findOneUser as jest.Mock).mockResolvedValue(mockUser);
    
            const response = await request(app).get('/users/1');
    
            expect(response.status).toBe(200);
            expect(response.body).toStrictEqual(mockUser);
        });
    });

    //test post new user
    describe('POST /users', () => {
        it('should add a new user', async () => {
          const newUser = {
            email: 'newuser@example.com',
            full_name: 'New User',
            username: 'newuser',
          };
          (addUser as jest.Mock).mockResolvedValue(newUser);
      
          const response = await request(app).post('/users').send(newUser);
      
          expect(response.status).toBe(200);
          expect(response.body).toEqual(newUser);
        });
      });

    //test get all post
    describe('GET /posts', () => {
        it('should return all posts', async () => {
            const mockPosts = [
                { id: 1, title: 'Post 1', content: 'Content 1' },
                { id: 2, title: 'Post 2', content: 'Content 2' },
            ];
            (findAllposts as jest.Mock).mockResolvedValue(mockPosts);

            const response = await request(app).get('/posts');

            expect(response.status).toBe(200);
            expect(response.body).toEqual(mockPosts);
        });
    });


    //test post new post
    describe('POST /posts', () => {
        it('should create a new post', async () => {
            const newPost = {
                title: 'New Post',
                content: 'New post content'
            };
            (addPost as jest.Mock).mockResolvedValue(newPost);

            const response = await request(app).post('/posts').send(newPost);

            expect(response.status).toBe(200);
            expect(response.body).toEqual(newPost);
        });
    });

   

    //test post new comment
    describe('POST /comment', () => {
        it('should create a new comment', async () => {
            const newComment = {
                comment_post_title: 1,
                comment_content: 'New comment',
            };
            (addComment as jest.Mock).mockResolvedValue(newComment);

            const response = await request(app).post('/comment').send(newComment);

            expect(response.status).toBe(200);
            expect(response.body).toEqual(newComment);
        });
    });

    //test delete user with id
    describe('DELETE /users/:id', () => {
        it('should delete the user with the specified id', async () => {
            const mockUserId = 1;
            (deleteUser as jest.Mock).mockResolvedValue(mockUserId);

            const response = await request(app).delete(`/users/${mockUserId}`);

            expect(response.status).toBe(200);
            expect(response.body).toStrictEqual({});
        });
    });

    //test delete post with id
    describe('DELETE /posts/:id', () => {
        it('should delete the post with the specified id', async () => {
            const mockPostId = 1;
            (deletePost as jest.Mock).mockResolvedValue(mockPostId);

            const response = await request(app).delete(`/posts/${mockPostId}`);

            expect(response.status).toBe(200);
            expect(response.body).toStrictEqual({});
        });
    });

 

});